Option Explicit

'// 유빌리티 계산기 by Ceruria (jubeat saucer CN : CERURIA*)
'// Licensed under GNU LGPL v2.0~
'// 참고: http://b.previrtu.com/62 (한국어, copious~saucer 버전)
'// 참고 2: http://yosh52.web.fc2.com/jubeat/jubility_matome.html (일본어, knit 버전)
'// 참고 3: http://yosh52.web.fc2.com/jubeat/jubility_copious/matome.html (일본어, copious~saucer 버전)

Dim Level_IN '// 입력 레벨
Dim Score_IN '// 입력 점수

Dim Result
Dim ResDisp


Level_IN = InputBox("곡의 레벨을 입력하세요 (1-10): ","레벨 입력","1")
Score_IN = InputBox("점수를 입력하세요 (0-1000000): ","점수 입력","700000")

Result = FormatNumber(GetJubility(Int(Score_IN),Int(Level_IN)),2) '// 소숫점 셋째 자리에서 반올림

ResDisp = "레벨: " & Int(Level_IN) & vbCrLf 
ResDisp = ResDisp & "점수: " & Int(Score_IN) & vbCrLf 
ResDisp = ResDisp & "환산된 유빌리티: " & Result

MsgBox ResDisp,64,"Result is"


'// 계산 함수 본체

Function GetJubility(Score,Level)
 Dim ReducedLevel
 ReducedLevel = Level - 1
 If Score < 700000 Then
  Score = Score / 700000
 ElseIf Score < 950000 Then
  Score = (Score - 650000) / 50000
 ElseIf Score < 980000 Then
  Score = (Score - 770000) / 30000
 Else
  Score = (Score - 840000) / 20000
 End If
 GetJubility = Score * (Score / 5 + ReducedLevel) / 8
End Function
